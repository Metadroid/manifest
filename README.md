Metadroid
===========
Metadroid is just an extension to COSP through which we 
are trying to provide a clean experience along with some important 
customization features. We have cherry-picked the features from many 
other projects and hence we are very thankful to them.

Credits
-------
* [**COSP**](https://github.com/cosp-project)
* [**Pixel Experience**](https://github.com/PixelExperience)
* [**Evolution X**](https://github.com/Evolution-X)
* [**Substratum**](https://github.com/Substratum)
* [**LineageOS**](https://github.com/LineageOS)

And many more that we may have not mentioned.


How to Build?
-------------

To initialize your local repository using the AospExtended trees, use a 
command like this:

```bash
  repo init -u https://gitlab.com/Metadroid/manifest -b pie
```
To initialize a shallow clone, which will save even more space & time, use a command like this:

```bash
  repo init --depth=1 -u https://gitlab.com/Metadroid/manifest -b pie
```
  
Then to sync up:
----------------

```bash
  repo sync --force-sync --current-branch --no-tags --no-clone-bundle --optimized-fetch --prune -j$(nproc --all) -q
```
Finally to build:
-----------------

```bash
  . build/envsetup.sh
  lunch meta_device_codename-userdebug
  mka bacon -j$(nproc --all)
```
## Report build issues
- You can reach us via [Telegram](https://t.me/expresslukechat)